﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using Microsoft.Extensions.Logging;
using XploRe.Runtime;


namespace XploRe.Logging
{

    /// <inheritdoc />
    /// <summary>
    /// <para>
    ///     Provides a generic enumeration with implicit conversion to <see cref="T:Microsoft.Extensions.Logging.EventId" /> 
    ///     instances for use with loggers.
    /// </para>
    /// <para>
    ///     The value of each enum member is fixed to an <c>int</c> and represents the numeric event ID. The name of the
    ///     event is automatically set to the corresponding field name.
    /// </para>
    /// <para>
    ///     Using the generic <see cref="T:XploRe.Logging.EventIdEnum`1" />, applications can define a central repository 
    ///     of event IDs with symbolic names and automatically generate resource informations for the Windows Event Log.
    /// </para>
    /// </summary>
    /// <typeparam name="TDerived">Derived enum implementation type.</typeparam>
    public abstract class EventIdEnum<TDerived> : GenericEnum<int, TDerived>
        where TDerived : EventIdEnum<TDerived>
    {

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Logging.EventIdEnum`1" /> instance with the given event ID.
        /// </summary>
        /// <param name="value">Event ID repesented by the enum member.</param>
        protected EventIdEnum(int value) : base(value)
        {
        }

        /// <summary>
        ///     Provides an implicit conversion to <see cref="EventId" /> instances for use with logger facilities.
        /// </summary>
        /// <param name="logEvent">
        ///     The <see cref="EventIdEnum{TDerived}" /> enumeration member instance to convert.
        /// </param>
        /// <returns>A new <see cref="EventId" /> instance from the enumeration member instance.</returns>
        public static explicit operator EventId(EventIdEnum<TDerived> logEvent)
        {
            if (logEvent == null) {
                return default(EventId);
            }

            return logEvent.ToEventId();
        }

        /// <summary>
        ///     Returns an <see cref="EventId" /> instance for the enumeration value represented by this
        ///     <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <returns>A new <see cref="EventId" /> instance from the enumeration member instance.</returns>
        public EventId ToEventId()
        {
            return new EventId(Value, Name);
        }

    }

}
