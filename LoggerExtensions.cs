﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.Extensions.Logging;


namespace XploRe.Logging
{

    /// <summary>
    ///     Provides logging extensions for <see cref="EventIdEnum{TDerived}" /> members.
    /// </summary>
    public static class LoggerExtensions
    {

        #region Event ID Expansion

        /// <summary>
        ///     Formats and writes a critical log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogCritical<T>(
            [NotNull] this ILogger logger,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogCritical(eventId.ToEventId(), eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes a debug log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogDebug<T>(
            [NotNull] this ILogger logger,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogDebug(eventId.ToEventId(), eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes an error log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogError<T>(
            [NotNull] this ILogger logger,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogError(eventId.ToEventId(), eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes an information log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogInformation<T>(
            [NotNull] this ILogger logger,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogInformation(eventId.ToEventId(), eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes a trace log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogTrace<T>(
            [NotNull] this ILogger logger,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogTrace(eventId.ToEventId(), eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes a warning log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogWarning<T>(
            [NotNull] this ILogger logger,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogWarning(eventId.ToEventId(), eventId.Description, args);
        }

        #endregion


        #region Event ID Expansion with Exception

        /// <summary>
        ///     Formats and writes a critical log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogCritical<T>(
            [NotNull] this ILogger logger,
            [CanBeNull]Exception exception,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogCritical(eventId.ToEventId(), exception, eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes a debug log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogDebug<T>(
            [NotNull] this ILogger logger,
            [CanBeNull]Exception exception,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogDebug(eventId.ToEventId(), exception, eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes an error log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogError<T>(
            [NotNull] this ILogger logger,
            [CanBeNull]Exception exception,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogError(eventId.ToEventId(), exception, eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes an information log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogInformation<T>(
            [NotNull] this ILogger logger,
            [CanBeNull]Exception exception,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogInformation(eventId.ToEventId(), exception, eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes a trace log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogTrace<T>(
            [NotNull] this ILogger logger,
            [CanBeNull] Exception exception,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogTrace(eventId.ToEventId(), exception, eventId.Description, args);
        }

        /// <summary>
        ///     Formats and writes a warning log message based on an <see cref="EventIdEnum{TDerived}" /> instance.
        /// </summary>
        /// <param name="logger">The <see cref="ILogger" /> instance to write to.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="eventId">
        ///     The <see cref="EventIdEnum{TDerived}" /> instance that holds the associated event ID and log message 
        ///     format string.
        /// </param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public static void LogWarning<T>(
            [NotNull] this ILogger logger,
            [CanBeNull]Exception exception,
            [NotNull] EventIdEnum<T> eventId,
            params object[] args)
            where T : EventIdEnum<T>
        {
            if (logger == null) {
                throw new ArgumentNullException(nameof(logger));
            }

            if (eventId == null) {
                throw new ArgumentNullException(nameof(eventId));
            }

            logger.LogWarning(eventId.ToEventId(), exception, eventId.Description, args);
        }

        #endregion

    }

}
